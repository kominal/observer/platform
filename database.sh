#!/bin/bash
docker run --rm --network infrastructure_production_global mongo:latest bash -c " \
    mongo --host kominal/mongodb01,mongodb02,mongodb03 ${STACK_NAME} --authenticationDatabase admin -u admin -p ${DATABASE_ADMIN_PASSWORD} --eval \"db.createUser({ user:'controller', pwd:'${DATABASE_PASSWORD}', roles:[{ role:'readWrite', db:'${STACK_NAME}_controller' }] })\"; \
    mongo --host kominal/mongodb01,mongodb02,mongodb03 ${STACK_NAME} --authenticationDatabase admin -u admin -p ${DATABASE_ADMIN_PASSWORD} --eval \"db.createUser({ user:'user-service', pwd:'${DATABASE_PASSWORD}', roles:[{ role:'readWrite', db:'${STACK_NAME}_user-service' }] })\"; \
"